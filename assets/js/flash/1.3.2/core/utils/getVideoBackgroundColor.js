flashCore.prototype.getVideoBackgroundColor = function(video, callback) {

    // Setup a canvas element in front of the video
    if(!video) return;
    video.style.opacity = 0;
    video.innerHTML = video.innerHTML + '';
    video.insertAdjacentHTML('afterend', '<canvas id="video-buffer" style="position: absolute; left: 0; top: 0;"></canvas>');
    var canvas = video.parentNode.querySelector('canvas#video-buffer');
    var buffer = canvas.getContext('2d');
    buffer.scale(2, 2);

    // Make the canvas same size than the video (with retina support)
    function fitCanvas() {
        canvas.setAttribute('width', video.offsetWidth * 2);
        canvas.setAttribute('height', video.offsetHeight * 2);
        canvas.style.width = video.offsetWidth + 'px';
        canvas.style.height = video.offsetHeight + 'px';
    } fitCanvas();
    window.onresize = function() { fitCanvas() };

    // Re-render the video on the canvas element to fix the colors
    function update() {
        fitCanvas();
        buffer.drawImage(video, 0, 0, video.offsetWidth * 2, video.offsetHeight * 2);
        window.requestAnimationFrame(update);
    }
    window.requestAnimationFrame(update);

    // Get the background color and clear the check interval
    var interval = setInterval(function() {
        if(!video.paused) {
            var _canvas = document.createElement('canvas');
            _canvas.width = 10;
            _canvas.height = 10;
            var _ctx = _canvas.getContext('2d');
            _ctx.drawImage(canvas, 0, 0, 10, 10, 0, 0, 10, 10);
            var p = _ctx.getImageData(0, 0, 10, 10).data;
            callback('rgb(' + p[60] + ', ' + p[61] + ', ' + p[62] + ')');
            clearInterval(interval);
        }
    }, 100);

};
